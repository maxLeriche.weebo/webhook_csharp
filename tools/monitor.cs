﻿using System;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tools
{
    public class monitor
    {
        static HttpClient client = new HttpClient();
        string url_webhook = "https://discordapp.com/api/webhooks/675046629010702388/QST7mRDRNcESfxd0AYm43OsuMY3D_gb1oa6XsUH-HETRK_IiPLBZKG4QNb2g8ApK-ZCS";
        string avatar = "https://cdn.shopify.com/s/files/1/0727/8355/products/eedda8c4-a8d5-48b8-92e6-74cf1bf541f1_800x.jpg?v=1576079578";
        string username = "MONITOR";
        string app = "monitor";

       

        public monitor(string app)
        {
            this.app = app ?? throw new ArgumentNullException(nameof(app));
            client.BaseAddress = new Uri(url_webhook);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public monitor(string url_webhook, string app)
        {
            this.url_webhook = url_webhook ?? throw new ArgumentNullException(nameof(url_webhook));
            this.app = app ?? throw new ArgumentNullException(nameof(app));
            client.BaseAddress = new Uri(url_webhook);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public monitor(string avatar, string username, string app)
        {
            this.avatar = avatar ?? throw new ArgumentNullException(nameof(avatar));
            this.username = username ?? throw new ArgumentNullException(nameof(username));
            this.app = app ?? throw new ArgumentNullException(nameof(app));
            client.BaseAddress = new Uri(url_webhook);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public monitor(string url_webhook, string avatar, string username, string app)
        {
            this.url_webhook = url_webhook ?? throw new ArgumentNullException(nameof(url_webhook));
            this.avatar = avatar ?? throw new ArgumentNullException(nameof(avatar));
            this.username = username ?? throw new ArgumentNullException(nameof(username));
            this.app = app ?? throw new ArgumentNullException(nameof(app));
            client.BaseAddress = new Uri(url_webhook);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> send_json(string lvl, string message)
        {
            string test = String.Format("{{ \" username \" : \"{0}\",\"avatar_url\": \"{1}\",\"embeds\":[{{ \"title\":\" {2}\" ,\"color\": \"13576456\",\"fields\":[{{\"name\": \"Level:\",\"value\": \" {3}\",\"inline\": \"false\"}},{{\"name\": \"Content\",\"value\": \"{4}\",\"inline\": \"true\"}}]}}]}}",
                                        username,
                                        avatar,
                                        app,
                                        lvl,
                                        message);
            return test;
        }

        public async Task<HttpStatusCode> send_log(string message)
        {
            string swapjson = await send_json("Log", message);
            var httpContent = new StringContent(swapjson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("",httpContent);
            response.EnsureSuccessStatusCode();
            return response.StatusCode;
        }

        public async Task<Uri> send_warning(string message)
        {
            string swapjson = await send_json("Warning", message);
            var httpContent = new StringContent(swapjson, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync("/", httpContent);
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }

        public async Task<Uri> send_debug(string message)
        {
            string swapjson = await send_json("Debug", message);
            var httpContent = new StringContent(swapjson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("/", httpContent);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

        public async Task<Uri> send_critical(string message)
        {
            string swapjson = await send_json("Critical", message);
            var httpContent = new StringContent(swapjson, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("/", httpContent);
            response.EnsureSuccessStatusCode();
            return response.Headers.Location;
        }

       


    }
}
