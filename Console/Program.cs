﻿using System;
using tools;
using System.Threading.Tasks;

namespace console
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Lancement de la procedure de test");
            monitor test = new monitor("c#");
            Console.WriteLine(test.send_log("je suis du c#").GetAwaiter().GetResult().ToString());
            Console.WriteLine("fin de la procedure");
        }
    }
}
